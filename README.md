Simple memcached is a pure socket implementation of memcached protocol.

Is supports namespaces https://code.google.com/p/memcached/wiki/NewProgrammingTricks and clusters.

This library is inspired by
http://blog.elijaa.org/?post/2010/05/21/Memcached-telnet-command-summary
http://docs.oracle.com/cd/E17952_01/refman-5.6-en/ha-memcached-interfaces-protocol.html
http://dustin.sallings.org/2010/06/29/memcached-vbuckets.html`

mecached protocol is described:
https://github.com/memcached/memcached/blob/master/doc/protocol.txt

Library support clusters using consistent hashing
http://en.wikipedia.org/wiki/Consistent_hashing

Implemented using library
https://github.com/pda/flexihash


Usage sample:
--------------------------

```
use \Simple\Memcached;

$client =  new Memcached();

$client->useredundancy(2); //number of redundant servers to use

//add servers with weight
//weight is usually a memory ratio, but can be anything
$client->add->server('localhost',11211, 1);
$client->add->server('localhost1',11211, 1);
$client->add->server('localhost2',11211, 2);
$client->add->server('localhost3',11211, 2);
$client->add->server('localhost4',11211, 4);
$client->add->server('localhost5',11211, 5);
$client->add->server('localhost6',11211, 6);


//use optional namespace
$client->namespace= 'mynamespace';

//set single value
$client->set("key","value");

//get single value
$client->get("key");

//set multiple values
$client->set(array(
                'key1'=> 'value1',
                'key2'=> 'value2',
                'key3'=> 'value3',
                'key4'=> 'value4',
                'key5'=> 'value5',
                'key6'=> 'value6',
                'key7'=> 'value7'
             ));


//get multiple values
$client->get(array(
                'key1',
                'key2',
                'key3',
                'key4',
                'key5',
                'key6',
                'key7'
             ));


//get primary server for key
$client->getserver('key');

//get primary and falback server for key
$client->getservers('key', 2);

$client->namespace->expire();

```
