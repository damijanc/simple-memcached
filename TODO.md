Todo list
==================

- [x] load dependencies trough composer
- [x] be composer friendly
- [x] do a code cleanup
- [ ] interfaces, interfaces, interfaces
- [ ] add error handling (timeout, network down etc.)
- [ ] finish the functinality
- [ ] break the lib into stat, cas, get/set part
- [ ] move socket out of the class 
- [ ] remove echo from lib
- [ ] remove configuration from file
- [ ] add yaml(xml,json, ini) configuration support
- [ ] add namespaces support
- [ ] add more tests
- [ ] write proper unit tests
- [ ] introduce automatic zipping
- [ ] introduce automatic serialization
- [ ] introduce multiple server support (clustering)
- [ ] introduce buckets
- [ ] add support for cas commands