<?php

/*this class is a placeholder for cluster functionality, but this may change since the feature is not implemented yet*/

namespace Simple\Memcached\Cluster;

class Cluster
{

    private $hash;

    public function __construct()
    {
        $this->hash = new \Flexihash();
    }

    public function add( $server, $weight = 1 )
    {
        $this->hash->addTarget( $server, $weight );
    }

    public function remove( $server )
    {
        $this->hash->removeTarget( $server );
    }

    public function replace()
    {

    }
} 