<?php
/**
 * Created by PhpStorm.
 * User: damijan.cavar
 * Date: 10.04.14
 * Time: 13:11
 */

namespace Simple\Memcached;

include_once 'Cluster.php';
include_once '../modules/flexihash/include/init.php';

use Simple\Memcached\Cluster;

class Memcached
{
    const STORED = 'STORED';

    private $port = 11211;
    private $host = 'localhost';
    private $socket;
    private $namespace = "";

    /**
     * @var Cluster
     */
    private $cluster;

    function __construct()
    {
        $this->cluster = new Cluster\Cluster();
    }

    public function __get($name)
    {
        if (property_exists($this, $name)) {
            return $this->{$name};
        }
    }

    public function __set($name, $value)
    {
        if (property_exists($this, $name)) {
            return $this->{$name} = $value;
        }
    }

    public function getStats()
    {
        $this->connect();

        $command = "stats\r\n";
        socket_write($this->socket, $command, strlen($command));
        echo($this->read());

        $this->disconnect();
    }

    protected function connect()
    {
        if (is_resource($this->socket) === false) {
            /* Create a TCP/IP socket. */
            $this->socket = socket_create(AF_INET, SOCK_STREAM, getprotobyname("TCP"));
            if ($this->socket === false) {
                echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
            } else {
                echo "OK.\n";
            }

            echo "Attempting to connect to '$this->host' on port '$this->port'...";
            $result = socket_connect($this->socket, $this->host, $this->port);
            if ($result === false) {
                echo "socket_connect() failed.\nReason: ($result) " . socket_strerror(
                        socket_last_error($this->socket)
                    ) . "\n";
            } else {
                echo "OK.\n";
            }
        }
    }

    protected function read()
    {

        $header_pattern = '~VALUE[ ].*[ ][0-9]*[ ][0-9]*[\r\n]~';
        $end_pattern = '~[\r\n]END[\r\n]~';


        $bytes = 0;
        $buf = "";
        $bytes_read = 0;
        $line = '';

        while (preg_match($end_pattern, $line) == 0) {

            if (is_resource($this->socket) === false) {
                error_log("socket has terminated unexpectedly");
            }
            $line = socket_read($this->socket, 2048, PHP_BINARY_READ);

            file_put_contents("read_values.txt", $line);

            // check for a socket_read error
            if ($line === false) {
                $errno = socket_last_error($this->socket);
                error_log("read(): socket_write() returned FALSE. Error $errno: " . socket_strerror($errno));

                return false;
            }

            $buf .= $line;



        }

        file_put_contents("buffer_value.txt", $buf);

        //get header information
        preg_match($header_pattern, $buf, $header);

        if (count($header) == 0) {
            error_log("read(): Header information not present");

            return false;
        }

        $matches = explode(" ", $header[0]);

        $matches = preg_replace('~[\r]~', '', $matches);
        $matches = preg_replace('~[\n]~', '', $matches);

        file_put_contents("matches_value.txt", print_r($matches, true));

        if (!is_string($matches[1])) error_log("read(): Header key is no string.", E_NOTICE);
        if (!is_numeric($matches[2])) error_log("read(): Header flags is no numeric.", E_NOTICE);
        if (!is_numeric($matches[3])) error_log("read(): Header bytes is no numeric.", E_NOTICE);

        if (is_string($matches[1]) && is_numeric($matches[2]) && is_numeric($matches[3])) {
            $key = $matches[1];
            $flags = $matches[2];
            $bytes = $matches[3];

            file_put_contents("datainfo_value.txt", "key=$key\nflags = $flags\nbytes=$bytes");

            $line = preg_replace($header_pattern, '', $line);
        } else {
            // something went wrong, we never received the header
            error_log("read(): Requested key(s) returned no values.", E_NOTICE);

            return false;
        }


        $value_pattern = '~(?:VALUE[ ].*[ ][0-9]*[ ][0-9]*[\r\n]*)([\s\S]{' . $bytes . '})[\s\S]*(?:[\r\n]END[\r\n]*)~';

        preg_match($value_pattern, $buf, $value);


        return $value[1];

    }

    protected function disconnect()
    {

        if (is_resource($this->socket) === true) {
            socket_close($this->socket);
        }

        if (is_resource($this->socket) === false) error_log("Socket successfully closed \n");
    }

    public function getSettings()
    {
        $this->connect();

        $command = "stats settings\r\n";
        socket_write($this->socket, $command, strlen($command));
        $this->read();

        $this->disconnect();
    }

    public function getItems()
    {
        $this->connect();

        $command = "stats items\r\n";
        socket_write($this->socket, $command, strlen($command));
        $this->read();

        $this->disconnect();
    }

    public function get($key)
    {
        $value = false;
        $this->connect();

        $command = "get $this->namespace$key\r\n";

        file_put_contents('get_command.txt', $command);

        socket_write($this->socket, $command, strlen($command));

        $value = $this->read();

        $this->disconnect();

        return $value;
    }

    public function delete($key)
    {

    }

    public function replace($key)
    {

    }

    public function append($key)
    {

    }

    public function prepend($key)
    {

    }

    public function incr($key)
    {

    }

    public function decr($key)
    {

    }

    public function flush_all()
    {

    }

    /**
     * @param $key
     * @param $value
     * @param $exptime  the actual value sent may either be
     * Unix time (number of seconds since January 1, 1970, as a 32-bit
     * value), or a number of seconds starting from current time. In the
     * latter case, this number of seconds may not exceed 60*60*24*30 (number
     * of seconds in 30 days); if the number sent by a client is larger than
     * that, the server will consider it to be real Unix time value rather
     * than an offset from current time.
     */
    public function set($key, $value, $exptime = 3600)
    {
        $this->connect();

        $len = strlen($value);

        $command = "set " . $this->namespace . $key . " 0 $exptime $len\r\n$value\r\n";

        file_put_contents('set_command.txt', $command);

        $this->write($command);

        $this->disconnect();
    }

    /**
     * Sets an array of values
     * @param array $values
     */
    public function aset(array $values)
    {
        foreach ($values as $key=>$value)
        {
            if (is_array($value)) {
                $this->set($key,$value[0],$value[1]);
            }else {
                $this->set($key,$value);
            }
        }
    }


    public function aget(array $keys)
    {

    }

    protected function write($command)
    {

        $cmd_len = strlen($command);
        $offset = 0;

        while ($offset < $cmd_len) {
            $result = socket_write($this->socket, substr($command, $offset, 2048), 2048);

            if ($result !== false) {
                $offset += $result;
            } else {
                if ($offset < $cmd_len) {
                    $errno = socket_last_error($this->sock);
                    error_log("set(): socket_write() returned FALSE. Error $errno: " . socket_strerror($errno));

                    return false;
                }
            }
        }
    }
} 