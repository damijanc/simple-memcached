<?php
/**
 * This test is a stub. It needs proper cleanup and functionality needs to be implemented in the first place.
 * This is practically a usage example and it should be replaced ASP.
 */
require_once '../vendor/autoload.php';

echo "Starting test \n";

echo "Creating a client\n";
$client = new \Simple\Memcached\Memcached();

//use optional namespace

$client->setNamespace('test::');


$client->getStats();

//set single value
//echo "Setting a value\n";
$client->set( "key", "value" );

//get single value
//echo "Getting a value\n";
$value = $client->get( "key" );


echo "Value returned is $value\n";


$file = file_get_contents( "Untitled.png" );

echo strlen( $file );

$client->set( "image", $file );

file_put_contents( "retrieved.png", $client->get( "image" ) );


//set multiple values
$client->aset( array(
    'key1' => array( 'value1', 400 ),
    'key2' => 'value2',
    'key3' => 'value3',
    'key4' => 'value4',
    'key5' => 'value5',
    'key6' => 'value6',
    'key7' => 'value7'
) );


//get multiple values
$client->aget( array(
    'key1',
    'key2',
    'key3',
    'key4',
    'key5',
    'key6',
    'key7'
) );
