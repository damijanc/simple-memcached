<?php
/*Purpose of this test is to test parsing of Retrieval command response
 *Every retrieval command ends with "END\r\n" to mark the end of response
 * More details in https://github.com/memcached/memcached/blob/master/doc/protocol.txt
 */
$matches = array();
$header = array();

$header_pattern = '~VALUE[ ].*[ ][0-9]*[ ][0-9]*[\r\n]';
$end_pattern = '~[\r\n]END[\r\n]~';
$value_pattern = '~(?:VALUE[ ].*[ ][0-9]*[ ][0-9]*[\r\n]*)([\s\S]{5})[\s\S]*(?:[\r\n]END[\r\n])~';

//real value example in format : VALUE [key] [flag] [length of value] [value]
$str = "VALUE mynamespace::key 0 5\r\nvalue\r\nEND\r\n";

//retrieve the footer
preg_match( $end_pattern, $str, $matches );
//retrieve the header
preg_match( $header_pattern, $str, $header );
//retrieve the value
preg_match( $value_pattern, $str, $value );

print_r( $value );