<?php

/* Purpose of this test is to test distribution on key trough server
 * It is not the exact science but it the servers with higher
 * weight should get more keys to store
 * Since keys are random, the distribution of them is also random
 * Please read: http://www.tomkleinpeter.com/2008/03/17/programmers-toolbox-part-3-consistent-hashing/
 * for better understanding of consistent hashing
 * */

require_once '../vendor/autoload.php';

//list of servers with weight (ratio of memory for i.e.)
$servers= array('server1'=>4, 'server2' => 2, 'server3'=> 1);

$hash = new \Flexihash();
$serverHits = array();

foreach($servers as $server=>$weight) {
    echo "Adding server: $server with weight: $weight\n";
    $hash->addTarget($server,$weight);
    $serverHits[$server] = 0;
}

echo "Running the keys lookup\n";
for($i=0;  $i<1000; $i++) {
    $key = rand();

    $s = $hash->lookup($key);
        $serverHits[$s] += 1;

}
//print the distribution
print_r($serverHits);
