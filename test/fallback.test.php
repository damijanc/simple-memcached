<?php

/* Purpose of this test is to test the scenario of adding a server to a cluster or a server failure
 * To achieve redundancy we should store data to at least 2 servers in a cluster, so if one fails we can use the fallback
 * The server with biggest weight will get statistically the most of the keys as primary server
 * Please read: http://www.tomkleinpeter.com/2008/03/17/programmers-toolbox-part-3-consistent-hashing/
 * for better understanding of consistent hashing
 */

require_once '../vendor/autoload.php';

//list of servers with weight (amount of memory for i.e.)
$servers = array( 'server1' => 4, 'server2' => 2, 'server3' => 1 );

$hash = new Flexihash();

foreach ( $servers as $server => $weight ) {
    echo "Adding server: $server with weight: $weight\n";
    $hash->addTarget( $server, $weight );
}

// simple lookup
//get server that stores the key
echo "Server: " . $hash->lookup( 'object-a' ) . " stored the key object-a \n"; // "cache-1"
echo "Server: " . $hash->lookup( 'object-b' ) . " stored the key object-b \n"; // "cache-2"

// add and remove
echo "Adding server: server4 with weight: 2048 and removing server: server1 \n";
$hash
    ->addTarget( 'server4', 2048 )
    ->removeTarget( 'server1' );

// lookup with next-best fallback (for redundant writes)
echo "Checking for server to store value: object with fallback\n";
print_r( $hash->lookupList( 'object', 2 ) ); // ["cache-2", "cache-4"]

// remove cache-2, expect object to hash to cache-4
echo "Removing server3 \n";
$hash->removeTarget( 'server3' );

echo "Getting fallback server: " . $hash->lookup( 'object' ) . " \n";